# GENERATOR
> Valid Fake Content Generator

## Version
1.0.2

**IMPORTANTE**: This data generator is designed to assist with the application testing process. It does not represent real information and should not be used for real registrations.

## What generates?
This extension generates the following items:
- CPF
- CURP

## Plugins
Use [randexp](https://www.npmjs.com/package/randexp) to generate regex content.

## License
MIT
